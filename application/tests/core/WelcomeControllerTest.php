<?php
require_once(__DIR__.'/../CITestCase.php');
require_once(__DIR__.'/../CsvFileIterator.php');

class WelcomeControllerTest extends CITestCase
{


    /**
     * @dataProvider additionProvider
     */

	public function testHome($a, $b, $expected)
	{
	    $this->requireController('Welcome'); // assuming you have a applications/controllers/Home.php
	    $CI = new Welcome();
	    $this->assertEquals($expected, $CI->add($a,$b));

	}


    public function additionProvider()
    {
        return array(
          'adding zeros' => array(0, 0, 0),
          'zero plus one' => array(0, 1, 1),
          'one plus zero' => array(1, 0, 1),
          'one plus one' => array(3, 1, 4),
            'one plus one' => array(12, 11, 33),
            'one plus one' => array(13, 21, 34),
            'one plus one' => array(14, 1, 15),
            'one plus one' => array(30, 1, 31),
            'one plus one' => array(22, 33, 55),
            'one plus one' => array(2, 1, 3)
        );
    }


    public function csvProvider()
    {
        return new CsvFileIterator(__DIR__.'/./data.csv');
    }

    public function testOne()
    {
        $this->assertTrue(TRUE);
    }

    /**
     * @depends testOne
     */
    public function testTwo()
    {
    }



     /**
     * @expectedException InvalidArgumentException
     */
    public function testException()
    {
    	throw new InvalidArgumentException("Error Processing Request", 1);
    	
      
    }


}
